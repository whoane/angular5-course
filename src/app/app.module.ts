import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router'
import { FormsModule } from '@angular/forms';

import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';

import { ProductService } from './service/product.service';
import { CartService } from './service/cart.service';

const appRouting: Routes = [
  {
    path: 'products',
    loadChildren: './product/product.module#ProductModule'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, // in child components we only need to import CommonModule
    HttpClientModule,
    CoreModule,
    RouterModule.forRoot(appRouting),
    FormsModule
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    ProductService,
    CartService
  ]
})
export class AppModule { }
