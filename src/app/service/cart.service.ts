import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/observable'
import 'rxjs/add/operator/map'

import { Fruit } from '../model/fruit.interface'

@Injectable()
export class CartService {

  private _cart: Fruit[] = [];
  private cartSubject: BehaviorSubject<Fruit[]> = new BehaviorSubject<Fruit[]>([])
  public cart$ = this.cartSubject.asObservable();

  constructor() {
    this.cart = localStorage.cart ? JSON.parse(localStorage.cart) : []
  }

  set cart(cart) {
    this._cart = cart
    this.cartSubject.next(this._cart)
  }

  get cart() {
    return this._cart
  }

  addProduct(product: Fruit) {
    this.cart = [...this.cart, product]
    localStorage.setItem('cart', JSON.stringify(this.cart))
  }

  countProducts(): Observable<number> {
    return this.cart$
      .map(products => {
         return products.length
      })
  }
}
