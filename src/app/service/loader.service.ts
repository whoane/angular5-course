import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

@Injectable()
export class LoaderService {

  private _visible: boolean

  private loaderSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loaderObservable = this.loaderSubject.asObservable();

  constructor() { }

  set visible(visible: boolean) {
    this._visible = visible
    this.loaderSubject.next(visible)
  }

  get visible() {
    return this._visible
  }

}
