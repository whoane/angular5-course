import { Injectable } from '@angular/core';
import { Fruit } from '../model/fruit.interface'
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/observable'
import 'rxjs/add/operator/do'

const PRODUCTS_API = 'http://localhost:3000/products'

@Injectable()
export class ProductService {

  private _products: Fruit[] = [];

  /*
  The difference with Subject is that it (Subject) doesn't jave a default value
  */
  private productsSubject: BehaviorSubject<Fruit[]> = new BehaviorSubject<Fruit[]>([]);
  public productsObservable = this.productsSubject.asObservable();

  private set products(products) {
    this._products = products
    this.productsSubject.next(products)
  }

  private get products(): Fruit[] {
    return this._products;
  }

  getProductById(productId: number) : Observable<Fruit> {
    return this.httpClient.get<Fruit>(`${PRODUCTS_API}/${productId}`)
  }

  addProduct(product: Fruit): Observable<Fruit> {
    return this.httpClient
      .post<Fruit>(PRODUCTS_API, product)
      .do(product => {
        this.products = [...this._products, product]
      })
  }

  deleteProduct(productId) {
    return this.httpClient
      .delete(`${PRODUCTS_API}/${productId}`)
      .do(product => {
        this.products = this.products.filter(product => product.id !== productId)
      })
  }

  constructor(private httpClient: HttpClient) {
    this.httpClient
      .get<Fruit[]>(PRODUCTS_API)
      .subscribe((products) => {
        this.products = products
      })
  }

  private setProducts(products: Fruit[]) {
    this._products = products
    this.productsSubject.next(products)
  }
}
