import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderService } from '../service/loader.service';
import { LoaderComponent } from '../loader/loader.component'

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoaderComponent
  ],
  providers: [
    LoaderService
  ],
  exports: [
    LoaderComponent
  ]
})
export class CoreModule { }
