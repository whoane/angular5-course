import { Component, OnInit } from '@angular/core';
import { Fruit } from '../../model/fruit.interface'
import { ProductService } from '../../service/product.service'
import { LoaderService } from '../../service/loader.service'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Fruit[] = []
  visible: boolean = true

  constructor(
    private productService: ProductService,
    private loaderService: LoaderService
  ) {
    this.productService.productsObservable
      .subscribe(products => {
        this.products = products
      });
    this.loaderService.loaderObservable
      .subscribe(spinnerVisible => {
        this.visible = !spinnerVisible
      })
  }

  deleteProduct(product) {
    this.productService.deleteProduct(product.id)
      .subscribe(() => {
        console.log('it works')
        setTimeout(() => {
          this.loaderService.visible = false
        }, 500);
      })
  }

  ngOnInit() {
  }

}
