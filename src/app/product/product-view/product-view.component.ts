import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Fruit } from '../../model/fruit.interface'
import { ProductService } from '../../service/product.service'
import { CartService } from '../../service/cart.service'

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  product: Fruit

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService
  ) {
    this.activatedRoute.params
      .subscribe((params: Params) => {
        console.log(params)
        const { productId } = params
        this.productService.getProductById(productId)
          .subscribe(product => {
            this.product = product
          })
      })
  }

  addProductToCart() {
    this.cartService.addProduct(this.product)
  }

  ngOnInit() {
  }

}
