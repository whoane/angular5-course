import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { Fruit } from '../../model/fruit.interface'

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Output('deleteProductForm') deleteProductForm: EventEmitter<Fruit> = new EventEmitter<Fruit>()
  @Input('id') id: number;
  @Input('name') name: string;
  @Input('description') description: string;
  @Input('image') image: string;

  constructor() { }

  deleteProduct($event) {
    $event.stopPropagation()
    this.deleteProductForm.emit({
      id: this.id,
      name: this.name,
      description: this.description,
      image: this.image
    })
  }

  ngOnInit() {
  }

}
