import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from '@angular/common';

import { CoreModule } from '../core/core.module'

import { ProductItemComponent } from './product-item/product-item.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductViewComponent } from './product-view/product-view.component';

const productRouting: Routes = [
  {
    path: ':productId',
    component: ProductViewComponent
  },
  {
    path: '',
    component: ProductListComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    CoreModule,
    RouterModule.forChild(productRouting)
  ],
  exports: [
    ProductItemComponent,
    ProductListComponent,
    ProductViewComponent
  ],
  declarations: [
    ProductItemComponent,
    ProductListComponent,
    ProductViewComponent
  ]
})
export class ProductModule { }
