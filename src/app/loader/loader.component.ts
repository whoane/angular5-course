import { Component, OnInit, Input } from '@angular/core';

import { LoaderService } from '../service/loader.service'

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  @Input('spinnerClass') spinnerClass: string = 'spinner-sm'
  public visible: boolean = false
 
  constructor(private loaderService: LoaderService) {
    loaderService.loaderObservable
      .subscribe(visible => {
        this.visible = visible
      })
  }

  ngOnInit() {
  }

}
