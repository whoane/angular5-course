import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductService } from './service/product.service'
import { CartService } from './service/cart.service'
import { LoaderService } from './service/loader.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('myForm') myForm: NgForm;

  title: string;
  cartCounter: number = 0

  constructor(
    public productService: ProductService,
    private cartService: CartService,
    private loaderService: LoaderService
  ) {
    this.cartService.countProducts()
      .subscribe(count => {
        this.cartCounter = count
      })
  }

  addNewProduct() {
    console.log(this.myForm)
    this.loaderService.visible = true
    this.productService.addProduct({
      name: this.myForm.value.title,
      description: 'Some description for kiwi',
      image: 'Orange.jpg'
    }).subscribe(product => {
      setTimeout(() => {
        this.loaderService.visible = false
      }, 500);
    })
  }
}
