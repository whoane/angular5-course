export interface Fruit {
  id?: number,
  name: string,
  description: string,
  image: string
}
